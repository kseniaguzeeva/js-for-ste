// while
let sum = 0;
let i = 0;
while (i < 5) {
    i++; // the expression is the same if I write i+1
    sum += i; // sum = sum + 1
};
console.log(sum);

// do .. while
let sum = 0;
let i = 0;
do {
    i++;
    sum += i;
} while (i < 5);
console.log(sum);

// for
let sum = 0;

for (let i = 0; i < 6; i++) {
    sum += i;
}

console.log(sum);

const arr = [1, 2, 3, 4, 5];

for (let i = 0; i < arr.length; i++) {
    console.log(arr[i]);
};


const arr = [1, 2, 3, 4, 5];
let sum = 0;

for (let i = 0; i < arr.length; i++) {
    sum += arr[i]; //sum = sum + arr[i]
}
console.log(sum);

// for .. in
const obj = {
    Name: "John",
    lastName: "Dou"
};

for (let key in obj) {
    console.log(key);
}

//or
const obj = {
    Name: "John",
    lastName: "Dou"
};

for (let key in obj) {
    console.log(obj[key]);
}
;

// for .. of
const arr = [1, 2, 3, 4, 5];

for (let el of arr) {
    console.log(el);
}

//or
const arr = [1, 2, 3, 4, 5];
let sum = 0;

for (let el of arr) {
    sum += el;
}

console.log(sum);