const x = 5;
if (x === 5) {
    console.log('this is true');
}
console.log('I am here!');

//we can skip curly brackets if we write the code in one line:
const x = 5;
if (x === 6) console.log('this is true');
console.log('I am here!');

// if we want to add else operator
const x = 5;
if (x === 6) {
    console.log('this is true');
}
else {
    console.log('else statement');
}

// else if
/**
 * 18-25 >- 18%
 * 26-40 >- 20%
 * 41-99 >- 30%
 */

const age = 5;
if (age >= 18 && age <= 25) {
    console.log('18%');
} else if (age >= 26 && age <= 40) {
    console.log('20%');
} else if (age >= 41 && age <= 99) {
    console.log('30%');
} else {
    console.log('error');
};

// Ternary operator
const num = 6;
if (num === 5) {
    console.log('true');
} else {
    console.log('false');
}
//using ternary operator:
(num === 5) ? console.log('true') : console.log('false'); // after ? we write action if it's true; after : we write action if it's false

//switch case
const title = 'Middle';
switch (title) {
    case ('Junior'):
        console.log('you are Junior');
        break;
    case ('Middle'):
        console.log('you are Middle');
        break;
    case ('Senior'):
        console.log('you are Senior');
        break;
    default:
        console.log('Sorry, I do not know who are you')
};