let a = 6;
console.log(a);


var b = 5;
console.log(5);


const a = 6;
console.log(a);

{
    const a = 6;
    console.log(a);
}
/**
 * Primitives
 */
let number = 4;
let string = 'Hello';
let boolean = true;
let nullValue = null;
let underfinedValue //underfined; - for variables that are not defined
let sym = Symbol();

/**
 * Strings
 */
const first = 'Hello';
const second = "World";
const result = first + '' + second;
console.log(result)
/**
 * Number
 */
const numFirst = 1500;
const numSecond = 0b11; //hexadecimal - consists of 16 symbols. It uses 10 symbols of decimal number i.e 0 to 9, and 6 additional values (A, B, C, D, E, F) to represent values (10, 11, 12, 13, 14, 15) respectively.
const numThird = 2.5; //decimal - Decimal number system consists of 10 symbols (0-9)

/**
 * Object
 */
const obj = {
    name: 'John Doe',
    age: 33,
    isEmployed: true,
    friends: ['Mike', 'Jack'],
    address: {
        street: 'Liepyno',
        house: 9
    }
};
console.log(obj.age); //we can use a dot to extract the value or squere brackets
console.log(obj['name']);

const property = 'age';
console.log(obj[property]);
console.log(obj.address.house);


/**
 * Arrays
 */
const arr = [1, "string", true, [1, 2, 3], { key: "value" }];

// to access to the element we have to write arr and the value number. The numberings begins from 0
console.log(arr.length);
console.log(arr[1]);
console.log(arr[4].key);

/**
 * Difference betweem primitives and complex data types:
 * A variable assigned to an object stores not the object itself,
 * but "its address in memory", in other words "a reference to it"
 */


let d = 5;
let b = d;

d = 7;
console.log(d);
console.log(b);

let arr = [1, 2, 3];
let arr2 = arr;

arr[0] = 10;
console.log(arr[0]);
console.log(arr2[0]);



//assignment
let x = 42;
// console.log('x=', x);

//arithmetic
let add = x + 4;
// console.log('sum.', add);
let sub = x - 4;
// console.log('sub.', sub);
let mul = x * 4;
// console.log('mul.', mul);
let div = x / 2;
// console.log('div.', div);
let mod = x % 5;
// console.log('mod.', mod);
let pow = x ** 1;
// console.log('pow.', pow);

// increment
let i = 1;
let k = ++i; // prefix increment - the execution result is k=2 and i=2. It happens because we have incremental value and only after that assignment to the variable "k"
console.log('k.', k);
console.log('i.', i);

let i = 1;
let k = i++; // prefix increment - If we change to the postfix, k=1 and i=2. It mean that assignment happens and only after that we increase the value of the variable I.
console.log('k.', k);
console.log('i.', i);


let J = 1;
j--; // decrement operator behaves the same way
--j;
console.log('j.', j);

// additional assignment
const INCREMENT_VALUE = 6;
let value1 = 100;
let value2 = 100;
let value3 = 100;
let value4 = 100;
let value5 = 100;

value1 += INCREMENT_VALUE;
// console.log('value1.', value1); // value1 = value1 + INCREMENT_VALUE
value2 += INCREMENT_VALUE;
// console.log('value2.', value2);
value3 += INCREMENT_VALUE;
// console.log('value3.', value3);

//comparison
let eq1 = 5 == 5; //non-strict equality
let eq2 = 5 == '5';
console.log('eq1 =', eq1);
console.log('eq2 =', eq2);

let seq1 = 5 === 5; //non-strict equality
let seq2 = 5 === '5';
console.log('strict equality =', seq1); //- the operator strict equality compares the value and the data type
console.log('strict equality =', seq2);

let uneq = 5 != '5'; //unequslity operator
let suneq = 5 !== '5';

//relational operators
let gt = 2 > 2;
let gte = 2 >= 2;
let lt = 3 < 3;
let lte = 3 <= 3;
console.log('gt =', gt);
console.log('gte =', gte);
console.log('lt =', lt);
console.log('lte =', lte);

//logical
let statement1 = 3 > 2; //true
let statement2 = 0 > 1; //false
//let antitruth = !statement1;  //operator antitruth simply converts the value of boolean type
//console.log('antitruth =', antitruth);

//let or = statement1 || statement2; // operator or returns true if at least one statement is true
//console.log('or =', or);

//let and = statement1 && statement2; //operator and produces the true value, if all values are true
//console.log('and =', and);

//typeof
let str = typeof 'John';
let nmb = typeof 3.14;
let bool = typeof false;
console.log('str =', str);
console.log('nmb =', nmb);
console.log('bool =', bool);

// Try catch
//const fs = require('fs');  // this function throws an error to the output when the file hasn;t been found
//const pathToDB = './database.json';
//function printUsers() {
//    const users = JSON.parse(fs.readFileSync(pathToDB));
//    return console.log(users);
//}
//
//printUsers();

function printUsers() { //this functiom throws our message when the file hasn't been found and also go to the second db after error and take data from there
    let users;
    try {
        users = JSON.parse(fs.readFileSync(pathToDB));
    } catch {
        console.log('Database is not found! Using backup database.');
        users = JSON.parse(fs.readFileSync(pathToBackupDB));
    } finally {
        console.log(users);
    }
};
